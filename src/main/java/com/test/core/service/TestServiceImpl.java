package com.test.core.service;

import com.test.core.dao.TestDao;
import com.test.core.model.TestInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("testService")
public class TestServiceImpl implements TestService{
    @Resource(name="testDao")
    private TestDao testDao;

    public void setTestDao(TestDao testDao) {
        this.testDao = testDao;
    }

    @Override
    public String getTestUser(String id) {
        return testDao.getTestById(id).toString();

    }

    @Override
    public List getTestUsers() {
        return testDao.getTestUsers();
    }


    @Override
    public void insert(TestInfo info) {
        testDao.insert(info);
    }

    @Override
    public void update(TestInfo info) {
        testDao.update(info);
    }

    @Override
    public void delete(TestInfo info) {
        testDao.delete(info);
    }
}
