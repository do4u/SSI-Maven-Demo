package com.test.core.service;

import com.test.core.model.TestInfo;

import java.util.List;
import java.util.Map;


public interface TestService {

    public String getTestUser(String id);
    public List getTestUsers();
    public void insert(TestInfo info);
    public void update(TestInfo info);
    public void delete(TestInfo info);

}
