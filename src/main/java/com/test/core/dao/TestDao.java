package com.test.core.dao;

import com.test.core.model.TestInfo;

import java.util.List;


public interface TestDao {

    public TestInfo getTestById(String id);

    public List getTestUsers();

    public void insert(TestInfo testInfo);

    public void update(TestInfo testInfo);

    public void delete(TestInfo testInfo);

}
