package com.test.core.dao;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.test.core.model.TestInfo;
import org.springframework.stereotype.Repository;

import com.test.common.dao.BaseDao;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("testDao")

public class TestDaoImpl extends BaseDao implements TestDao{

    @Resource(name="sqlMapClient")
    private SqlMapClient sqlMapClient;



    @Override
    public TestInfo getTestById(String id) {
        Map map = new HashMap<String,String>();
        map.put("id",id);
        return (TestInfo)getObject("test.getUser",map);
    }

    @Override
    public List getTestUsers() {
        List list = getSqlMapClientTemplate().queryForList("test.getUsers",new Object());
        return list;
    }

    @Override
    public void insert(TestInfo testInfo) {
        getSqlMapClientTemplate().insert("test.insert", testInfo);
    }

    @Override
    public void update(TestInfo testInfo) {
        getSqlMapClientTemplate().update("test.update", testInfo);
    }

    @Override
    public void delete(TestInfo testInfo) {
        getSqlMapClientTemplate().delete("test.delete", testInfo);
    }
}
