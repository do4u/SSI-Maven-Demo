package com.test.core.action;


import javax.annotation.Resource;

import com.test.core.model.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.test.common.action.BaseAction;
import com.test.common.util.RequestUtil;
import com.test.core.service.TestService;

import java.util.List;


@Controller
public class TestAction extends BaseAction{
    public final static Logger logger = LoggerFactory.getLogger(TestAction.class);

    private String out;

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

    private TestService testService;

	@Resource(name="testService")
	public void setTestService(TestService testService) {
		this.testService = testService;
	}


	public String execute(){
		//基本参数
		String format = RequestUtil.getString(request, "test", "JSON");
        out = testService.getTestUser("test").toString();
        logger.info("exe执行完毕");
		return SUCCESS;
	}

    public String insert(){
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        TestInfo testInfo = new TestInfo();
        testInfo.setId(id);
        testInfo.setName(name);
        testInfo.setPassword(password);
        testService.insert(testInfo);
        logger.info("insert执行完毕");
        return SUCCESS;
    }

    public String update(){
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        TestInfo testInfo = new TestInfo();
        testInfo.setId(id);
        testInfo.setName(name);
        testInfo.setPassword(password);
        testService.update(testInfo);
        logger.info("update执行完毕");
        return SUCCESS;
    }

    public String delete(){
        String id = request.getParameter("id");
        TestInfo testInfo = new TestInfo();
        testInfo.setId(id);
        testService.delete(testInfo);
        logger.info("delete执行完毕");
        return SUCCESS;
    }

    public String queryList(){
        List<TestInfo> list = testService.getTestUsers();
        StringBuffer sb = new StringBuffer("<br>");
        for(TestInfo testInfo: list){
            sb.append(testInfo.toString()+"<br>");
        }
        out = sb.toString();
        logger.info("queryList执行完毕");
        return SUCCESS;
    }
}
