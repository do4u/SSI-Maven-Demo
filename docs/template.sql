CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `batch` varchar(10) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `commmet` varchar(255) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `create_person` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO template (id, batch, name, commmet, create_date, create_person, status) VALUES (1, '2015', 'report', '全面风险报告模版', '2015-07-30', 'chenyu', '0');
INSERT INTO template (id, batch, name, commmet, create_date, create_person, status) VALUES (2, '2015', 'reportB', '全面风险报告2', '2015-07-07', 'chenyu', '0');
commit;