CREATE TABLE users
(
    id VARCHAR(50) PRIMARY KEY NOT NULL,
    name VARCHAR(50) DEFAULT '' NOT NULL,
    password VARCHAR(50) DEFAULT '' NOT NULL,
    description VARCHAR(100) DEFAULT '' NOT NULL
);

INSERT INTO users (id, name, password, description) VALUES ('213', 'chenyu', '234', '');
INSERT INTO users (id, name, password, description) VALUES ('3', '3', '3', '3');
INSERT INTO users (id, name, password, description) VALUES ('ewr', '111', '111', '');
INSERT INTO users (id, name, password, description) VALUES ('sdf', 'chenyu', '234', '');
INSERT INTO users (id, name, password, description) VALUES ('test', 'ceshi ', 'pw', '�����û�');
INSERT INTO users (id, name, password, description) VALUES ('test2', '2', '2', '2');
commit;