
CREATE TABLE `template_node` (
  `id` int(11) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `level_num` varchar(20) DEFAULT NULL,
  `superior` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `notes` varchar(20) DEFAULT NULL,
  `text` text,
  `is_optional` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (1, 1, 0, '0',  0 ,'全面风险报告模版题目', '报告标题', '标题文字', 0);
INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (2, 1, 1, '1',   1,'1.第一个一级标题', '一级标题', '一级标题', 0);
INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (3, 1, 2, '1_1', 2,'1.1第一个二级标题', '二级标题', '二级标题', 0);
INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (4, 1, 2, '1_2', 2,'1.2第二个二级标题', '二级标题2', '二级标题2', 0);
INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (5, 1, 1, '2',   1,'2.第二个一级标题', '一级标题2', '一级标题2', 0);
INSERT INTO template_node (id, template_id, level, level_num,superior,title, notes, text, is_optional) VALUES (6, 1, 2, '2_1', 5,'2.1第三个二级标题', '二级标题3', '二级标题3', 1);

select * from template_node;